function findTheStudentLiveInAustralia(arrayOfObjects) {
  if (!Array.isArray(arrayOfObjects)) {
    return "The given data is not valid";
  }

  let listOfIndividuals = [];

  for (let index = 0; index < arrayOfObjects.length; index++) {
    if (
      arrayOfObjects[index].isStudent &&
      arrayOfObjects[index].country === "Australia"
    )
      listOfIndividuals.push(arrayOfObjects[index]);
  }

  return listOfIndividuals;
}

module.exports = findTheStudentLiveInAustralia;
