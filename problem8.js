function printCountryAndCity(arrayOfObjects) {
  if (!Array.isArray(arrayOfObjects)) {
    return "The given data is not valid";
  }

  for (let index = 0; index < arrayOfObjects.length; index++) {
    console.log(
      "City = " +
        arrayOfObjects[index].city +
        ", Country = " +
        arrayOfObjects[index].country
    );
  }
}
module.exports = printCountryAndCity;
