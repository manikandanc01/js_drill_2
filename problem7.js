function printNamesAndEmail(arrayOfObjects, age) {
  if (!Array.isArray(arrayOfObjects)) {
    return "The given data is not valid";
  }

  for (let index = 0; index < arrayOfObjects.length; index++) {
    if (arrayOfObjects[index].age === 25) {
      console.log(
        arrayOfObjects[index].name + " " + arrayOfObjects[index].email
      );
    }
  }
}

module.exports = printNamesAndEmail;
