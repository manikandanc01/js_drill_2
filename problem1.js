function findEmailAddressesOfAll(arrayOfObjects) {
  if (!Array.isArray(arrayOfObjects)) {
    return "The given data is not valid";
  }

  let listOfEmailAddresses = [];

  for (let index = 0; index < arrayOfObjects.length; index++) {
    listOfEmailAddresses.push(arrayOfObjects[index].email);
  }

  return listOfEmailAddresses;
}

module.exports = findEmailAddressesOfAll;
