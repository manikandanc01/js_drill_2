function findTheHobbies(arrayOfObjects, age) {
  if (!Array.isArray(arrayOfObjects)) {
    return "The given data is not valid";
  }

  let listOfHobbies = [];

  for (let index = 0; index < arrayOfObjects.length; index++) {
    if (arrayOfObjects[index].age === age) {
      listOfHobbies.push(arrayOfObjects[index].hobbies);
    }
  }

  return listOfHobbies;
}

module.exports = findTheHobbies;
