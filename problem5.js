function printAllPersonsAge(arrayOfObjects, position) {
  if (!Array.isArray(arrayOfObjects)) {
    return "The given data is not valid";
  }

  for (let index = 0; index < arrayOfObjects.length; index++) {
    console.log(arrayOfObjects[index].age);
  }
}

module.exports = printAllPersonsAge;
