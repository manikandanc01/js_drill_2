function printFirstHobbyOfAllPersons(arrayOfObjects) {
  if (!Array.isArray(arrayOfObjects)) {
    return "The given data is not valid";
  }

  for (let index = 0; index < arrayOfObjects.length; index++) {
    console.log(arrayOfObjects[index].hobbies[0]);
  }
}

module.exports = printFirstHobbyOfAllPersons;
